# flake8: noqa
from jobard_client.client import JobardClient
from jobard_client.core.exceptions import JobardError
from jobard_client.core.models import JobOrderSubmit
from jobard_client.core.types import Cluster, State

# create jobard client (link to a jobard api)
client = JobardClient('http://localhost:8000/')

try:
    # submit a job order and retrieve job order id
    job_order_id: int = client.submit(
        JobOrderSubmit(
            command=['echo'],
            arguments=[[i] for i in range(50)],
            cores=1,
            split='/10',
            memory='2G',
            walltime='00:30:00',
            cluster=Cluster.LOCAL,
            ssh='localhost'
        )
    )

    print('Job order id : {0}'.format(job_order_id))
    job_order_stat = None
    for progress in client.progress(job_order_id=job_order_id, delay_beetween_loops=10):
        print(progress.json())
        job_order_stat = progress
    print('Final job order state : {0}'.format(job_order_stat.state))

    # print jobs in failure
    print('*' * 70)
    print('List of jobs in error'.format(job_order_id))
    for job in client.all_jobs(job_order_id=job_order_id, states=[State.FAILED]):
        print(job.json())

    # print job arguments in success
    print('*' * 70)
    print('List of arguments in success'.format(job_order_id))
    for arguments in client.all_arguments(job_order_id=job_order_id, states=[State.SUCCEEDED]):
        print(arguments)

except JobardError as error:
    print('=' * 70)
    print(error)
