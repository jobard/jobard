#!/bin/bash

########## Environment variables ##########

source jobard_stack.env

########## Preparation ##########
docker network create --driver=overlay jobard

########## Deployment ##########

docker stack deploy --compose-file jobard_stack.yml jobard 

