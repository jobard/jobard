# jobard

## Installation

```bash
conda create -n jobard python=3.9
conda activate jobard
pip install https://gitlab.ifremer.fr/jobard/jobard
```

## Usage

### Prepare configuration

```bash
mkdir -p _build/{conf,log}
sed "s|{{CONDA_PREFIX}}|$CONDA_PREFIX|g" templates/alembic.ini.tpl > _build/conf/alembic.ini
sed "s|{{CONDA_PREFIX}}|$CONDA_PREFIX|g" templates/daemon.yml.tpl > _build/conf/daemon.yml
sed -i "s|{{PROJECT_ROOT_PATH}}|$PWD|g" _build/conf/daemon.yml
````

### Run database and API

You can run postgresql database and jobard api via a compose file or manually

#### From compose file

```bash
cd compose
docker-compose -p jobard up --force-recreate
```

#### Manually

- start postgresql server
  
  ```bash
  docker run -p 5432:5432 --name jobard_db -e POSTGRES_PASSWORD=jobard -e POSTGRES_USER=jobard postgres:14.2
  ```

- create or upgrade database schema
  
  ```bash
  conda activate jobard
  cd _build/conf/
  alembic upgrade head
  ```
  
- stop and remove container 

  ```bash
  docker rm -f jobard_db
  ```

- launch the api  with your IDE or with this command : 

  ```bash
  python manual/start_api.py 
  ```

Launch your browser to : <http://127.0.0.1:8000/docs/>

### Launch daemon

```bash 
jobard-daemon --config _build/conf/daemon.yml --debug run
```

### Launch example

```bash
python example.py
```