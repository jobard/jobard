import uvicorn  # noqa: WPS433

uvicorn.run('jobard_api.main:app', host='127.0.0.1', port=8000)