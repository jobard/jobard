database:
  dsn: postgresql://jobard:jobard@localhost:5432/jobard

ssh_hosts:
  localhost:
    host: localhost
    app_path: {{CONDA_PREFIX}}/bin

executors:
  processor:
      # put path to current directory to easily see the logs
      remote_app_log_chroot_default: {{PROJECT_ROOT_PATH}}/_build/log