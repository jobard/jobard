import base64
import subprocess
from pathlib import Path
from typing import Union, Dict, List, Sequence

from airflow import AirflowException
from airflow.models.baseoperator import BaseOperator
from jobard_client.client import JobardClient
from jobard_client.core.exceptions import JobardError
from jobard_client.core.jobs.models import JobOrderSubmit
from jobard_client.core.core_types import State


class JobardOperator(BaseOperator):
    """
    This class implements a Jobard Operator allowing running a command on a distributed remote cluster
    and using the Jobard API.
    """

    # Add field which can be rendered with Jinja templating.
    template_fields: Sequence[str] = ("jobard_args_list",)

    def __init__(
            self,
            jobard_token: str,
            jobard_command: List[str],
            jobard_url: str = 'http://localhost:8000/',
            jobard_name: str = None,
            jobard_args: [List[str]] = None,
            jobard_args_file: str = None,
            jobard_cores: int = 1,
            jobard_split: str = None,
            jobard_memory: str = '1G',
            jobard_walltime: str = '00:30:00',
            jobard_priority: int = 1,
            jobard_connection: str = None,
            jobard_extra: List[str] = None,
            jobard_env_extra: Dict[str, str] = None,
            jobard_interpolate_command: bool = False,
            jobard_max_concurrency: int = 1,
            **kwargs,
    ) -> None:
        super().__init__(**kwargs)
        self.joborder_id = None
        self.client = JobardClient(jobard_url)
        self.jobard_command = jobard_command
        self.jobard_name = jobard_name
        self.jobard_args_list = jobard_args
        self.jobard_args_file = Path(jobard_args_file) if jobard_args_file is not None else None
        self.jobard_cores = jobard_cores
        self.jobard_split = jobard_split
        self.jobard_memory = jobard_memory
        self.jobard_walltime = jobard_walltime
        self.jobard_priority = jobard_priority
        self.jobard_connection = jobard_connection
        self.jobard_extra = jobard_extra
        self.jobard_env_extra = jobard_env_extra
        self.jobard_interpolate_command = jobard_interpolate_command
        self.jobard_max_concurrency = jobard_max_concurrency
        self.token = jobard_token

    def build_command_arguments(self) -> [List[str]]:
        """
        Build command arguments to submit from input file or args list.
        Returns:
            The command arguments.
        """
        args = None
        if self.jobard_args_list is not None:
            args = self.jobard_args_list
            self.log.info(f'Setting command args : {self.jobard_args_list}')
        elif self.jobard_args_file is not None:
            if self.jobard_args_file.is_file():
                self.log.info(f'Setting command args from input file: {str(self.jobard_args_file)}')
                with open(self.jobard_args_file) as fd:
                    files_args = fd.readlines()
                    if len(files_args) == 0:
                        error = f'Command arguments file is empty : {self.jobard_args_file}'
                        self.log.error(error)
                        raise AirflowException(error)
                    else:
                        args = [[file.rstrip()] for file in files_args]
            else:
                error = f'Input file does not exist : {self.jobard_args_file}'
                self.log.error(error)
                raise AirflowException(error)
        return args

    def execute(self, context: Dict = None) -> Union[bytes, str]:
        """
        Execute the task:
            - Submit the Job Order.
            - Wait the end of the Job Order by looping over Job Order progress.
        Returns:
            The Job Order status.
        """
        result: Union[bytes, str]
        if self.jobard_command is None:
            raise AirflowException('Jobard operator error: command not specified. Aborting.')
        self.log.info(f'Jobard command : {self.jobard_command}')

        # Build command arguments to submit from input file or args list
        args = self.build_command_arguments()

        self.log.info(f'Submitting Job Order')
        jo_state = None
        try:
            # submit a job order
            self.joborder_id = self.client.submit(
                JobOrderSubmit(
                    command=self.jobard_command,
                    name=self.jobard_name,
                    # arguments=[["value1"], ["value2"],... ["value100"]],
                    arguments=args,
                    interpolate_command=self.jobard_interpolate_command,
                    cores=self.jobard_cores,
                    split=self.jobard_split,
                    memory=self.jobard_memory,
                    walltime=self.jobard_walltime,
                    priority=self.jobard_priority,
                    connection=self.jobard_connection,
                    job_extra=self.jobard_extra,
                    env_extra=self.jobard_env_extra,
                    max_concurrency=self.jobard_max_concurrency
                ),
                token=self.token
            )

            # wait for the end of the command
            self.log.info(f'Wait for the end of the job order : {self.joborder_id}')
            jo_state = self.wait_for_job_end(joborder_id=self.joborder_id)

            # retrieve job status
            exit_status = self.exit_status(jo_state)
            result = str(jo_state)
            self.log.info(f'Exit status : {exit_status}')
        except JobardError as error:
            exit_status = 1
            result = error
            self.log.error(error)

        # list failed execution logs
        if self.joborder_id is not None and jo_state == State.FAILED:
            self.list_failed_job_logs(joborder_id=self.joborder_id)

        # exit if error
        if exit_status > 0:
            raise AirflowException('Jobard operator ended with error')

        return result

    @staticmethod
    def exit_status(job_state: State) -> int:
        """
        Computes the exit status
        Args:
            job_state : The Job Order state.

        Returns:
            The exit status.
        """
        status = 1
        if job_state == State.SUCCEEDED:
            return 0
        return status

    def wait_for_job_end(self, joborder_id: int, interval: int = 10) -> State:
        """
        Follow the submitted Job Order by looping until the end of the Job Order.
        Args:
            joborder_id : The Job Order id.
            interval : The time interval between two iteration.
        Returns:
            The Job Order state.
        """
        job_order_stat = None
        self.log.info(f'Follow Job Order : {joborder_id}')
        for progress in self.client.progress(job_order_id=joborder_id, delay_beetween_loops=interval, token=self.token):
            job_order_stat = progress
            self.log.info(f'Job Order progress : {progress.json()}')
        # TODO CR : voir si on doit mettre un timeout ?
        self.log.info(f'Final job order state : {job_order_stat.state}')
        return job_order_stat.state

    def list_failed_job_logs(self, joborder_id: int):
        """
        Logs the content of the failed jobs logs.
        Args:
            joborder_id : The Job Order id.
        """
        self.log.warning('--------------The processing failed : check out the following logs:----------------')
        for job in self.client.all_jobs(job_order_id=joborder_id, states=[State.FAILED], token=self.token):
            if job.log_path_stdout is not None and Path(job.log_path_stdout).is_file():
                result = subprocess.run(['cat', job.log_path_stdout], stdout=subprocess.PIPE).stdout.decode('utf-8')
                if result != '':
                    self.log.warning(job.log_path_stdout)
                    self.log.warning(result)
            if job.log_path_stderr is not None and Path(job.log_path_stderr).is_file():
                result = subprocess.run(['cat', job.log_path_stderr], stdout=subprocess.PIPE).stdout.decode('utf-8')
                if result != '':
                    self.log.warning(job.log_path_stderr)
                    self.log.warning(result)
        self.log.warning('-----------------------------------------------------------------------------------')

    def on_kill(self) -> None:
        """
        Called when the task is cancelled.
        Cancel gracefully the job order.
        """

        self.log.info(f'Cancelling Job Order')
        if self.joborder_id is not None:
            try:
                # cancel a job order
                self.client.cancel(job_order_id=self.joborder_id, token=self.token)
            except JobardError as error:
                self.log.error(error)
                raise AirflowException(str(error))
